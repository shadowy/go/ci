#!/usr/bin/env bash
echo "----------- start test -----------"
echo "BUILD_IMAGE=${BUILD_IMAGE} ${SRC}"

echo "docker run --rm --name build -v $SRC:/src ${BUILD_IMAGE} /test.sh"
docker run --rm --name build -v $SRC:/src ${BUILD_IMAGE} "/test.sh"
err=$?
if [[ "${err}" != 0 ]]; then
 echo "exit ${err}"
 echo "----------- end test -----------"
 exit ${err}
fi

echo "----------- end test -----------"

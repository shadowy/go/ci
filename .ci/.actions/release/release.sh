#!/usr/bin/env bash
echo "----------- start release -----------"
echo "BUILD_IMAGE=${BUILD_IMAGE} ${SRC}"

echo "docker run --rm --name build -v $SRC:/src ${BUILD_IMAGE} /release.sh"
docker run --rm --name build -v $SRC:/src ${BUILD_IMAGE} "/release.sh"
err=$?
if [[ "${err}" != 0 ]]; then
 echo "exit ${err}"
 echo "----------- end release -----------"
 exit ${err}
fi

echo "----------- end release -----------"

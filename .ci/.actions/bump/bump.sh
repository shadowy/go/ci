#!/usr/bin/env bash
echo "----------- start bump -----------"
source ${ROOT}/.utils/git/prepare-for-commit.sh
source ${ROOT}/.utils/git/git-get-version.sh

patch=$((patch+1))
echo "new version=${major}.${minor}.${patch}"
git tag -a "v${major}.${minor}.${patch}" -m "version ${major}.${minor}.${patch}"
git-chglog -o CHANGELOG.md
git tag -d "v${major}.${minor}.${patch}"
git add CHANGELOG.md
git commit -m "CHANGELOG.md"
commit_hash=`git rev-parse HEAD`
git tag -a "v${major}.${minor}.${patch}" -m "version ${major}.${minor}.${patch}" ${commit_hash}
git push origin HEAD:master --follow-tags
echo "----------- end bump -----------"

#!/usr/bin/env bash
echo "----------- start lint -----------"
echo "BUILD_IMAGE=${BUILD_IMAGE} ${SRC}"

echo "docker run --rm --name build -v $SRC:/src ${BUILD_IMAGE} /lint.sh"
docker run --rm --name build -v $SRC:/src ${BUILD_IMAGE} "/lint.sh"
err=$?
if [[ "${err}" != 0 ]]; then
 echo "exit ${err}"
 echo "----------- end lint -----------"
 exit ${err}
fi

echo "----------- end lint -----------"

#!/usr/bin/env bash
echo "----------- start lint badge -----------"

curl -X GET 'https://goreportcard.com/checks?repo=gitlab.com/${CI_PROJECT_PATH}'
err=$?
if [[ "${err}" != 0 ]]; then
 echo "exit ${err}"
 echo "----------- end lint badge-----------"
 exit ${err}
fi

echo "----------- end lint badge-----------"

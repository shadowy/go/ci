#!/usr/bin/env bash
#!/usr/bin/env bash
echo "----------- start build -----------"
echo "BUILD_IMAGE=${BUILD_IMAGE} ${SRC}"

echo "docker run --rm --name build -v $SRC:/src ${BUILD_IMAGE} /build.sh"
docker run --rm --name build -v $SRC:/src ${BUILD_IMAGE} "/build.sh"
err=$?
if [[ "${err}" != 0 ]]; then
 echo "exit ${err}"
 echo "----------- end build -----------"
 exit ${err}
fi

echo "----------- end build -----------"

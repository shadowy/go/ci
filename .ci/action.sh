#!/usr/bin/env bash
echo "----------- start action -----------"

ROOT=`dirname $0`
SRC=`pwd`

case "$1" in
    test)
        source ${ROOT}/.utils/docker/prepare-build-image.sh
        source ${ROOT}/.actions/test/test.sh
        ;;
    lint)
        source ${ROOT}/.utils/docker/prepare-build-image.sh
        source ${ROOT}/.actions/lint/lint.sh
        ;;
    lint-badge)
        source ${ROOT}/.actions/lint/lint-badge.sh
        ;;
    build)
        source ${ROOT}/.utils/docker/prepare-build-image.sh
        source ${ROOT}/.actions/build/build.sh
        ;;
    bump)
        source ${ROOT}/.actions/bump/bump.sh
        ;;
    release)
        source ${ROOT}/.utils/docker/prepare-build-image.sh
        source ${ROOT}/.actions/release/release.sh
        ;;
esac
echo "----------- end action -----------"

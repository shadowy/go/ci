#!/usr/bin/env bash
echo "----------- start prepare -----------"
if [ "$(ls -A /src)" ]
then
     echo "----------- start copy files -----------"
     cp -r /src/* /go/src/${SRC_PATH}
     echo "----------- end copy files -----------"
else
    echo "/src is empty"
fi
echo "----------- end prepare -----------"

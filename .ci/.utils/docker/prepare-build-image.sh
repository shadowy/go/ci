#!/usr/bin/env bash
CHECKSUM=($(md5sum ./go.mod))
BUILD_IMAGE=${CI_REGISTRY_IMAGE}/build:${CHECKSUM}
echo "BUILD_IMAGE=${BUILD_IMAGE}"

echo "----------- start prepare build image -----------"

source ${ROOT}/.utils/docker/login.sh

if [[ "$(docker images -q ${BUILD_IMAGE} 2> /dev/null)" == "" ]]; then
  docker pull ${BUILD_IMAGE}
  err=$?
  if [[ "${err}" != 0 ]]
  then
    echo "* start build image ${BUILD_IMAGE}"
    echo "docker build -t ${BUILD_IMAGE} -f ${ROOT}/docker/Dockerfile . --build-arg SRC=gitlab.com/${CI_PROJECT_PATH}"
    docker build -t ${BUILD_IMAGE} -f ${ROOT}/docker/Dockerfile . --build-arg SRC=gitlab.com/${CI_PROJECT_PATH}
    echo "* finished build image ${BUILD_IMAGE}"
    echo "* start push image ${BUILD_IMAGE}"
    docker push ${BUILD_IMAGE}
    err=$?
    if [[ "${err}" != 0 ]]; then
      echo "exit ${err}"
      echo "----------- end prepare build image -----------"
      exit ${err}
    fi
    echo "* end push image ${BUILD_IMAGE}"
  fi
fi
echo "----------- end prepare build image -----------"

#!/usr/bin/env bash

echo "----------- start docker login -----------"

docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
err=$?
if [[ "${err}" != 0 ]]; then
    echo "exit ${err}"
    echo "----------- end docker login -----------"
    exit ${err}
fi

echo "----------- end docker login -----------"

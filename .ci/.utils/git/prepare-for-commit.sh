#!/usr/bin/env bash
echo "----------- start prepare for commit -----------"
REPOSITORY_URL=""
regex="http[s]*:\/\/.*?@(.*)"
if [[ $CI_REPOSITORY_URL =~ $regex ]]; then
    REPOSITORY_URL="git@${BASH_REMATCH[1]}"
    REPOSITORY_URL="${REPOSITORY_URL/\//:}"
fi
echo ${REPOSITORY_URL}

eval $(ssh-agent -s)
echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add - > /dev/null
mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts
git config --global user.name "${GITLAB_USER_NAME}"
git config --global user.email "${GITLAB_USER_EMAIL}"
git remote set-url origin "${REPOSITORY_URL}"
git remote -v
echo "----------- end prepare for commit -----------"

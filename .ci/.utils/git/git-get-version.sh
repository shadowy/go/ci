#!/usr/bin/env bash
echo "----------- start git get version -----------"
major=1
minor=0
patch=0

version=`git describe --abbrev=0 --tags $(git rev-list --tags --max-count=1)`
err=$?
if [[ "${err}" == 0 ]]
then
    echo "parse version"
    version=${version#"v"}
    arrIN=(${version//\./ })
    major=${arrIN[0]}
    minor=${arrIN[1]}
    patch=${arrIN[2]}
fi
echo "current version=${major}.${minor}.${patch}"
echo "----------- end git get version -----------"

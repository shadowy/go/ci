#!/usr/bin/env bash
echo "---------------- start download ----------------"
curl -L "https://gitlab.com/shadowy/go/ci/-/jobs/artifacts/master/raw/ci.tar.gz?job=build" | tar -xz
echo "---------------- end download ----------------"
